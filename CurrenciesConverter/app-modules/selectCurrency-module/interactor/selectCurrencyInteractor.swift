//
//  selectCurrencyInteractor.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import Alamofire
import ObjectMapper
import RealmSwift

class selectCurrencyInteractor:PresenterToInteractorSelectCurrencyProtocol {
    
    var presenter: InteractorToPresenterSelectCurrencyProtocol?
    
    func fetchCurrencyList() {
        if let path = Bundle.main.path(forResource: "currencies", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? NSArray {
                    let arrayObject = Mapper<SelectCurrencyModel>().mapArray(JSONArray: jsonResult as! [[String : Any]]);
                    self.presenter?.selectCurrencyFetchSuccess(currenciesList: arrayObject)
                }
            } catch {
                // handle error
            }
        }
    }
    func addCurrencyPair(objCurrencyPair: CurrencyPairModel) {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        print(urls)
        RealmManager.addOrUpdate(model: CurrencyPairModel.className(), object: objCurrencyPair) { (error) in
            if (error == nil) {
               self.presenter?.addCurrencyPairSuccess(result: true)
            }
            else {
                self.presenter?.addCurrencyPairSuccess(result: false)
            }
        }
    }
    
    func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(CurrencyPairModel.self).max(ofProperty: "pid") as Int? ?? 0) + 1
    }

}
