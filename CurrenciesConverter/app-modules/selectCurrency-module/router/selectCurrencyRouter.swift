//
//  selectCurrencyRouter.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import UIKit

class selectCurrencyRouter:PresenterToRouterSelectCurrencyProtocol {
    
    func createSelectCurrencyModule(arySelectedCurrenciesPair:Array<CurrencyPairModel>) -> selectCurrencyVC {
        let view = selectCurrencyRouter.mainstoryboard.instantiateViewController(withIdentifier: "selectCurrencyVC") as! selectCurrencyVC
        
        let presenter: ViewToPresenterSelectCurrencyProtocol & InteractorToPresenterSelectCurrencyProtocol = selectCurrencyPresenter()
        let interactor: PresenterToInteractorSelectCurrencyProtocol = selectCurrencyInteractor()
        let router:PresenterToRouterSelectCurrencyProtocol = selectCurrencyRouter()
        
        view.selectCurrencyPresenter = presenter
        view.arySelectedCurrenciesPair = arySelectedCurrenciesPair
        presenter.view = view
        presenter.router = router
        presenter.interactor = interactor
        interactor.presenter = presenter
        return view
    }
    
    static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    
    func animateSelectCurrencyScreen(viewController: UIViewController) {
        UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 1, options: .allowUserInteraction, animations: {
        }) { _ in
            let transition = CATransition()
            transition.duration = 0.35
            transition.type = CATransitionType.push
            transition.subtype = CATransitionSubtype.fromRight
            transition.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut)
            viewController.view.window!.layer.add(transition, forKey: kCATransition)
        }
        
    }
    
}
