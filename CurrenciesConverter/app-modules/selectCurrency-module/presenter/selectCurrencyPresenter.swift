//
//  selectCurrencyPresenter.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//
import Foundation
import UIKit

class selectCurrencyPresenter:ViewToPresenterSelectCurrencyProtocol{
    
    var view: PresenterToViewSelectCurrencyProtocol?
    
    var interactor: PresenterToInteractorSelectCurrencyProtocol?
    
    var router: PresenterToRouterSelectCurrencyProtocol?
    
    func startFetchingCurrencies() {
        interactor?.fetchCurrencyList()
    }
    func animateSelectCurrencyController(viewController:UIViewController) {
        router?.animateSelectCurrencyScreen(viewController: viewController)
    }
    func addCurrencyPair(objCurrencyPair: CurrencyPairModel) {
        interactor?.addCurrencyPair(objCurrencyPair: objCurrencyPair)
    }

}

extension selectCurrencyPresenter:InteractorToPresenterSelectCurrencyProtocol{
    func addCurrencyPairSuccess(result: Bool) {
        view?.onAddCurrencyResponse(result: result)
    }
    
    
    func selectCurrencyFetchSuccess(currenciesList: Array<SelectCurrencyModel>) {
        view?.onSelectCurrencyResponseSuccess(currencyModelArrayList: currenciesList)
    }
    
    
}
