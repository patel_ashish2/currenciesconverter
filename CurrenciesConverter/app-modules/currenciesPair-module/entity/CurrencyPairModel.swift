//
//  CurrencyPairModel.swift
//  CurrenciesConverter
//
//  Created by Ashish Patel on 2019/07/13.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import Foundation
import RealmSwift

@objcMembers
class CurrencyPairModel: Object {
    @objc dynamic var pid = 0
    @objc dynamic var exchangePair : String? = nil
    @objc dynamic var sourceCurrencyName: String? = nil
    @objc dynamic var sourceCurrencyCode: String? = nil
    @objc dynamic var destinationCurrencyName: String? = nil
    @objc dynamic var destinationCurrencyCode: String? = nil
    @objc dynamic var currencyExchangeRate: String? = nil

    override static func primaryKey() -> String? {
        return "pid"
    }
    public func incrementID() -> Int {
        let realm = try! Realm()
        return (realm.objects(CurrencyPairModel.self).max(ofProperty: "pid") as Int? ?? 0) + 1
    }

}

