//
//  selectCurrencyPresenterTests.swift
//  CurrenciesConverterTests
//
//  Created by Ashish Patel on 2019/07/16.
//  Copyright © 2019 Ashish Patel. All rights reserved.
//

import XCTest
import Cuckoo
@testable import CurrenciesConverter

class currenciesPresenterTests: XCTestCase {

    private var currencyPresenter: currenciesPairPresenter!
    let mockView = MockPresenterToViewProtocol()
    let mockInteractor = MockPresenterToInteractorProtocol()
    let mockRouter = MockPresenterToRouterProtocol()


    override func setUp() {
        currencyPresenter = currenciesPairPresenter()
        currencyPresenter.router = mockRouter
        currencyPresenter.interactor = mockInteractor
        currencyPresenter.view = mockView

    }

    func testStartFetchingCurrenciesPair() {

        stub(mockInteractor) { stub in
            when(stub).getLatestExchangeRates().thenDoNothing()
        }

        currencyPresenter.startFetchingCurrenciesPair()

        verify(mockInteractor).getLatestExchangeRates()
    }

    func testShowSelectCurrencyController() {
        let testArray = Array<CurrencyPairModel>()

        stub(mockRouter) { stub in
            when(stub).pushToSelectCurrencyScreen(navigationConroller: any(), arySelectedCurrenciesPair: any()).thenDoNothing()
        }

        currencyPresenter.showSelectCurrencyController(navigationController: UINavigationController(), currenciesPair: testArray)

        verify(mockRouter).pushToSelectCurrencyScreen(navigationConroller:any(), arySelectedCurrenciesPair:any())
    }

    func testCurrenciesPairFetchedSuccess() {
        let currenciesPairModelArray = Array<CurrencyPairModel>()

        stub(mockView) { stub in
            when(stub).showCurrenciesPair(currenciesPairArray: any()).thenDoNothing()
        }

        currencyPresenter.currenciesPairFetchedSuccess(currenciesPairModelArray: currenciesPairModelArray)

        verify(mockView).showCurrenciesPair(currenciesPairArray: any())
    }

}
